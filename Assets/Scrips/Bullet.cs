﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    float destroyTime=5f;
    [SerializeField]
    float damage = 25f;
    void Start()
    {
        Invoke("DestroyAmmo",destroyTime);
    }
    private void DestroyAmmo()
    {
        Destroy(gameObject);
    }
    private void OnTriggerEnter(Collider collision)
    {
        Enemy enemy = collision.GetComponent<Enemy>();
        if (enemy != null)
        {
            enemy.TakeDamage(damage);
            Destroy(gameObject);
        }
      
    }
}
