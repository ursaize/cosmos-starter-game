﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonAudio : MonoBehaviour
{
    public AudioSource myFx;
    public AudioClip clickFX;
    public AudioClip hoverFX;

    public void ClickSound()
    {
        myFx.PlayOneShot(clickFX);
    }
    public void HoverSound()
    {
        myFx.PlayOneShot(hoverFX);
    }
}