﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Enemy : MonoBehaviour
{
    [SerializeField]
    float maxHealth = 1000f;
    [SerializeField]
    float curHealth = 0f;
    [SerializeField]
    GameObject healthObj;
    [SerializeField]
    Text healthText;
    
    void Start()
    {
        curHealth = maxHealth;
    }
    void Update()
    {
        healthObj.transform.localScale = new Vector3(curHealth / 1000, 1, 1);
        healthText.text = "+" + curHealth.ToString("0");

        if (curHealth >= 1000)
        {
            curHealth = 1000;
        }
    }

    void Death()
    {
        Destroy(gameObject);
    }

    public void TakeDamage(float damage)
    {
        curHealth -= damage;
        if(curHealth<=0)
        {
            Death();
            Application.LoadLevel(2);
        }
    }

}
