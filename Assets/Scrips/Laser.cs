﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class Laser : MonoBehaviour
{
    [SerializeField] float laserOffTime = 2f;
    [SerializeField] float maxDistance = 200f;
    [SerializeField] float fireDelay = 2f;
    LineRenderer lr;
    bool canFire;

    void Awake()
    {
        lr = GetComponent<LineRenderer>();
    }

    void Update()
    {
        FireLaser(transform.forward * maxDistance);

    }
    void Start()
    {
        lr.enabled = false;
        canFire = true;
    }
    public void FireLaser(Vector3 targetPosition)
    {
        if (canFire)
        {
            lr.SetPosition(0, transform.position);
            lr.SetPosition(1, targetPosition);
            lr.enabled = true;
            canFire = false;
        }
        Invoke("TurnOffLaser", laserOffTime);
        Invoke("CanFire", fireDelay);
    }
    void TurnOffLaser()
    {
        lr.enabled = false;
        canFire = true;
    }
    public float Distance
    {
        get { return maxDistance; }
    }
    void CanFire()
    {
        canFire = true;
    }
}

