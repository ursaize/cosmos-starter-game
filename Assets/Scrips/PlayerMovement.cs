﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float movementSpeed = 80f;
    [SerializeField] float turnSpeed = 90f;
    [SerializeField] Thruster[] thruster;
    Transform myT;


    private MobileController mContr;
    void Awake()
    {
        mContr = GameObject.FindGameObjectWithTag("Joystick").GetComponent<MobileController>();
        myT = transform;
    }
   void Update()
    {
        Turn();
        Thrust();
    }

    void Turn()
    {
        float yaw = turnSpeed * Time.deltaTime * mContr.Horizontal();
        float pitch = turnSpeed * Time.deltaTime * mContr.Pitch();
        float roll = turnSpeed * Time.deltaTime * mContr.Roll();


        myT.Rotate(pitch, yaw, -roll);
    }

    void Thrust()
    {

        if (mContr.Vertical() > 0)
            myT.position += myT.forward * movementSpeed * Time.deltaTime * mContr.Vertical();
        if ((Input.GetKeyDown(KeyCode.W)) || (mContr.move == true))
        foreach (Thruster t in thruster)
            t.Activate();
        else if(Input.GetKeyUp(KeyCode.W) || (mContr.move == false))
            foreach (Thruster t in thruster)
                t.Activate(false);
    }
}
