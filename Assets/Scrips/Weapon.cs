﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField]
    Transform bullet;
    [SerializeField]
    int BulletForce = 10000;
    [SerializeField]
    int Clip = 10;
    [SerializeField]
    AudioClip Fire;
    [SerializeField]
    AudioClip Reloads;
    bool gun = false;
    bool ready = true;
    void Update()
    {

        if ((gun == true) && Clip > 0)
        {
            Transform BulletInstance = (Transform)Instantiate(bullet, GameObject.Find("Spawn_left").transform.position, Quaternion.identity);
            BulletInstance.GetComponent<Rigidbody>().AddForce(transform.forward * BulletForce);
            BulletInstance = (Transform)Instantiate(bullet, GameObject.Find("Spawn_right").transform.position, Quaternion.identity);
            BulletInstance.GetComponent<Rigidbody>().AddForce(transform.forward * BulletForce);
            Clip -= 2;
            GetComponent<AudioSource>().PlayOneShot(Fire);
            gun = false;

        }
        else if ((gun == true) && Clip <= 0 && ready == true)
        {
            ready = false;
            GetComponent<AudioSource>().PlayOneShot(Reloads);
            Invoke("Reload", 2f);
            gun = false;
        }
       
    }
    public void ButtonClick()
    {
        ready = true;
        gun = true;
    }
    public void Reload()
    {
       Clip = 10;
        
    }
   
}
